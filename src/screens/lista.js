import React from 'react';

import { TableDrivers } from '../components';

class ListaScreen extends React.Component {

    addDriver = () => {
        this.props.switchScreen('edicion');
    }

    render() {
        return(
            <div className="section">
                <div className="section-header">
                    <div className="section-title">
                        Drivers List
                    </div>
                    <div className="section-tools">
                        <button className="btn-toolbar" onClick={this.addDriver}>Add Driver</button>
                    </div>
                </div>
                <TableDrivers />
            </div>
        );
    }
}

export default ListaScreen;