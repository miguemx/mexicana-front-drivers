import React from "react";

import ReactModal from "react-modal"

import "../styles/modal.css";

class AppModal extends React.Component {

    render() {
        let botones = [];
        if ( typeof(this.props.botones) === 'object' ) {
            for ( let i=0; i<this.props.botones.length; i++ ) {
                botones.push(
                    <button onClick={this.props.botones[i].action} key={i} className="btn-toolbar">
                        {this.props.botones[i].title}
                    </button>
                );
            }
        }

        return(
            <ReactModal
                isOpen={this.props.open}
                contentLabel={this.props.title}
                ariaHideApp={false}
                className="modal-modal"
                overlayClassName="modal-overlay" >
                <div className="modal-title">
                    {this.props.title}
                </div>
                <div className="modal-body">
                    {this.props.mensaje}
                </div>
                <div className="modal-footer">
                    {botones}
                </div>
            </ReactModal>
        );
    }

}

export default AppModal;