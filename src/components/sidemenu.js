import React from 'react';

class SideMenu extends React.Component {

    switchScreen = (pantalla) => {
        this.props.switchScreen(pantalla);
    }
    
    render() {
        let classNameMenu = 'side-menu-container-hidden';
        if ( this.props.visible === true ) {
            classNameMenu = 'side-menu-container';
        }
        return(
            <div className={classNameMenu}>
                <button className="btn-menu-item" onClick={() => this.switchScreen('listado')}>
                    Drivers list
                </button>
                <button className="btn-menu-item" onClick={() => this.switchScreen('edicion')}>
                    Add new driver
                </button>
            </div>
        );
    }
}

export default SideMenu;