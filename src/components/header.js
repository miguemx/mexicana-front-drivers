import React from 'react';

const logo = require('../img/logo.png');

class Header extends React.Component {
    render() {
        return(
            <div className="header-container">
                <div>
                    <button className="btn-icono btn-menu" onClick={this.props.switchSideMenu}></button>
                </div>
                <div>
                    <img src={logo} alt="Mexicana Car Service" height="25" className="img-logo" />
                </div>
                <div> Drivers Management</div>
            </div>
        );
    }
}

export default Header;