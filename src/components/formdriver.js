import React from 'react';

import "../styles/forms.css";

class FormDriver extends React.Component {

    state ={
        id: "1",
        car: "1001",
        imei: "12403889",
        pin: "3889",
        status: "ACTIVE",
        online: "0",
        owner: "0",
        drvdn: null,
        licence: "MXNPUE2016",
        licenceType: null,
        licenceExp: null,
        tlcLic: null,
        tlcExpDate: null,
        beginDate: "0000-00-00",
        tlcUrineExp: null,
        vehicleId: "1001",
        vehicleType: "1",
        plate: "UAZ8146",
        year: "2017",
        model: "SONIC",
        make: "CHEVROLET",
        color: "GRAY",
        owned: "1",
        regExpDate: null,
        insurancePolicy: null,
        insuranceExpDate: null,
        insuranceCompany: null,
        vehicleVi: null,
        tlcDiamond: null,
        diamondExpDate: null,
        inspectionDate: null,
        registredName: null,
        personId: "4",
        surname: "SILVA-DRIVER",
        givenname: "MIGUEL",
        address: "APENINOS 20",
        phone: "0",
        cellphone: "000000000",
        email: "correo@correo.com",
        city: null,
        state: null,
        zip: null,
        birth: null,
        ssNumber: null,
        usCitizen: null
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.name === 'isGoing' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }

    render() {
        return(
            <div className="form-container">
                <div className="form-row">
                    <label>
                        Car Number:
                        <input name="car" type="number" value={this.state.car} onChange={this.handleInputChange} />
                    </label>
                </div>
            </div>
        );
    }

}

export default FormDriver;