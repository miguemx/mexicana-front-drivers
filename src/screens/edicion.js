import React from 'react';

import { FormDriver, AppModal } from '../components';

class EdicionScreen extends React.Component {

    state = {
        modalSave: false,
        modalCancel: false,
    }

    promptCancel = () => {
        this.setState({modalCancel: !this.state.modalCancel});
    }

    cancelar = () => {
        this.props.switchScreen('listado');
    }

    guardar = () => {

    }

    render() {
        return(
            <div className="section">
                <div className="section-header">
                    <div className="section-title">
                        Register / Modify driver information
                    </div>
                    <div className="section-tools">
                        <button className="btn-toolbar" onClick={this.save}>Save</button>
                        <button className="btn-toolbar" onClick={ this.promptCancel }>Cancel</button>
                    </div>
                </div>
                <FormDriver />
                <AppModal 
                    open={this.state.modalCancel} 
                    title="Cancel driver edit" 
                    mensaje="Are you sure you want to cancel current edit?" 
                    botones={[
                        {action: this.promptCancel, title:"No"}, 
                        {action: this.cancelar, title: "Yes"}
                    ]} />


            </div>
        );
    }
}

export default EdicionScreen;