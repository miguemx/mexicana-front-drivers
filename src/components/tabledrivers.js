import React from 'react';

import '../styles/tables.css';

import { BASEURL } from '../constants';

class TableDrivers extends React.Component {

    state = {
        drivers: [],
    }

    componentDidMount() {
        this.getDrivers();
    }

    getDrivers = async () => {
        let data = {};
        let response = await fetch(BASEURL + 'Drivers/', {
            method: 'POST', 
            body: JSON.stringify(data), 
			headers:{
				'Content-Type':'application/json',
				'Accept':  '*'
			}
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
        .then( (response) => { return response } );

        if ( response.status === 'ok' ) {
            if ( response.data.totalRegistros > 0 ) {
                let drivers = response.data.registros
                this.setState({drivers});
            }
        }
    }

    render() {
        return(
            <table className="tabla-datos">
                <thead>
                    <tr>
                        <th>Car</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Online</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.drivers.map( (driver)=>{
                        return(
                        <tr key={driver.id}>
                            <td>{driver.car}</td>
                            <td>{driver.surname} {driver.givenname}</td>
                            <td>{driver.status}</td>
                            <td>{driver.online}</td>
                            <td>&nbsp;</td>
                        </tr>)
                    } )}
                </tbody>
            </table>
        );
    }

}

export default TableDrivers;