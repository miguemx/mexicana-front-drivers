import React from 'react';

import { ListaScreen, EdicionScreen } from './screens';
import { Header, SideMenu } from './components';

import './styles/layout.css';
import './styles/button.css';

class App extends React.Component {
	state ={
		menuVisible: false,
		pantalla: 'listado',
	}

	switchSideMenu = () => {
		let menuVisible = !this.state.menuVisible;
		this.setState({menuVisible});
	}

	switchSceen = (pantalla) => {
		this.setState({pantalla});
	}

	render() {
		let pantalla = <ListaScreen />;
		switch ( this.state.pantalla ) {
			case 'listado': pantalla = <ListaScreen switchScreen={this.switchSceen} />; break;
			case 'edicion': pantalla = <EdicionScreen switchScreen={this.switchSceen} />; break;
			default: pantalla = <ListaScreen switchScreen={this.switchSceen} />; break;
		}
		
		return(
			<div className="page-container">
				<Header switchSideMenu={this.switchSideMenu} />
				<div className="body-container">
					<SideMenu visible={this.state.menuVisible} switchScreen={this.switchSceen} />
					<div className="work-area-container">
						{pantalla}
					</div>
				</div>
				<div className="footer-container">{'(c) Mexicana Car Service'}</div>
			</div>
		);
	}
}

export default App;
